public class Cadena{
   String cadena;
   public Cadena(){   cadena="";}
   
   public Cadena(String s){
       this.cadena =s;
   }
   
   public String toString(){
        return cadena;
   }
   
   public String invertir(String s){
        int n = s.length();
        String ret="";
        for(int i=n-1; i>=0; i--){
          ret+=s.charAt(i);
        }
        return ret;
    }
    
    public String vacio(String s){
        String ret="";
        return ret;
    }
    
    public String agregarfinal(String s){
        String ret = s;
        return ret+"c";
    }
    
    public String agregarinicio(String s){
        String ret = s;
        return "x"+ret;
    }
    
    public String borrarfinal(String s){
        int n = s.length();
        String ret;
        ret=s.substring(0,n-1);
        
        return ret;
    }
    
    public String borrarinicio(String s){
        int n = s.length();
        String ret;
        ret=s.substring(1);
        
        return ret;
    }
}