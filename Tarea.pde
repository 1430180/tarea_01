Cadena str;
Boton b1 ,b2 , b3, b4, b5, b6, b7;
PFont p;
String men, men2;
public void setup(){
    size(570,200);
    smooth();
    str = new Cadena("Mexico");
    p = createFont("Arial",18);
    textFont(p);
    b1 = new Boton(20,25, "Invierte");
    b2 = new Boton(160,25, "borrarInicio");
    b3 = new Boton(300,25, "Vacio");
    b4 = new Boton(440,25, "agregarFinal");
    b5 = new Boton(20,75, "agregarInicio");
    b6 = new Boton(160,75, "Llena");
    b7 = new Boton(300,75, "borrarFinal");
    men2 ="Aqui va el mensaje";
}

public void draw(){
     background(127);
     fill(0,255,0);
     text(str.toString(),20,height-60);
     b1.dibujar();
     b2.dibujar();
     b3.dibujar();
     b4.dibujar();
     b5.dibujar();
     b6.dibujar();
     b7.dibujar();
     fill(255);
     text(men2,20,height-30);
}

public void mousePressed(){
    men = b1.click(mouseX, mouseY);
    if(men.equals("Invierte")){
       str.cadena = str.invertir(str.cadena);
       men2="Invierte";
    }else
       men2="Esta afuera";
    men = b2.click(mouseX, mouseY);
    if(men.equals("borrarInicio")){
       str.cadena = str.borrarinicio(str.cadena);
       men2="borrarInicio";
    }
    men = b3.click(mouseX, mouseY);
    if(men.equals("Vacio")){
       str.cadena = str.vacio(str.cadena);
       men2="Vacio";
    }
    men = b4.click(mouseX, mouseY);
    if(men.equals("agregarFinal")){
       str.cadena = str.agregarfinal(str.cadena);
       men2="agregarFinal";
    }
    men = b5.click(mouseX, mouseY);
    if(men.equals("agregarInicio")){
       str.cadena = str.agregarinicio(str.cadena);
       men2="agregarInicio";
    }
    men = b6.click(mouseX, mouseY);
    if(men.equals("Llena")){
       str.cadena = str.invertir(str.cadena);
       men2="Llena";
    }
    men = b7.click(mouseX, mouseY);
    if(men.equals("borrarFinal")){
       str.cadena = str.borrarfinal(str.cadena);
       men2="borrarFinal";
    }
    
}